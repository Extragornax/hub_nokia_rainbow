#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#ifndef PSTR
 #define PSTR
#endif

#define NB_LED 64
#define PIN 6

const int power = 2;
const int powerBuzzer = 13;
const int ledUltrason = 8;
const int inputUltrason = 4;
const int outputUltrason = 7;
const int buzzer = 10;
const int buzzer2 = 12;
const int interrupt = 3;
enum croche { simpleC = 250, doubleC = 125};
int arr[] = {1320, 1176, 740, 830, 1110, 990, 588, 660, 990, 880, 555, 660, 880};
croche delayA[] = {doubleC, doubleC, simpleC, simpleC, doubleC, doubleC, simpleC, simpleC, doubleC, doubleC, simpleC, simpleC, simpleC};
long duration;
long distance;
bool openA;
const int distanceMax = 15;

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(1, NB_LED, PIN,
  NEO_MATRIX_TOP     + NEO_MATRIX_RIGHT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_PROGRESSIVE,
  NEO_GRB            + NEO_KHZ800);

void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(40);
  
  pinMode(power, OUTPUT);
  pinMode(inputUltrason, INPUT);
  pinMode(outputUltrason, OUTPUT);
  pinMode(ledUltrason, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(buzzer2, OUTPUT);
  pinMode(powerBuzzer, OUTPUT);
  pinMode(interrupt, INPUT);
  duration = 0;
  distance = 0;
}

void loop() {
  digitalWrite(power, HIGH);
  
  digitalWrite(outputUltrason, HIGH);
  delayMicroseconds(10);
  digitalWrite(outputUltrason, LOW);
  duration = pulseIn(inputUltrason, HIGH);
  distance = duration / 58.2;
  if (distance >= 0 && distance <= distanceMax)
  {
    digitalWrite(ledUltrason, HIGH);
    if (openA)
    {
      for (int i = 0 ; i < 13 ; i++)
      {
        tone(buzzer, arr[i]);
        delay(delayA[i]);
      }
    }
    rainbow(0.5); //Random de X seconds
    //theaterChase(matrix.Color(random(0, 255), random(0, 255), random(0, 255)), 50); //Stroboscope random colors
  }
  else
  {
    digitalWrite(ledUltrason, LOW);
    noTone(buzzer);
    noTone(buzzer2);
  }
  if (digitalRead(interrupt))
  {
    digitalWrite(powerBuzzer, LOW);
    openA = false;
  }
  else
  {
    digitalWrite(powerBuzzer, HIGH);
    openA = true;
  }
  if (distance <= 0  || distance > distanceMax)
  {
    colorWipe(matrix.Color(0, 0, 0), 1);
  }
  //delay(50);
}

void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<294; i++) {
      matrix.drawPixel(0, i, c);
      matrix.show();
      delay(wait);
  }
}

void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {
    for (int q=0; q < 3; q++) {
      for (int i=0; i < NB_LED; i=i+3)
        matrix.drawPixel(0, i+q, c);
      matrix.show();
      delay(wait);
      for (int i=0; i < NB_LED; i=i+3)
        matrix.drawPixel(0, i+q, 0);
    }
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<NB_LED; i++) {
      matrix.drawPixel(0, i, Wheel((i+j) & 255));
    }
    matrix.show();
    delay(wait);
  }
}

uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
   return matrix.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return matrix.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
   WheelPos -= 170;
   return matrix.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}
